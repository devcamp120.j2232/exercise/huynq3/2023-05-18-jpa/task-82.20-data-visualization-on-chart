package com.devcamp.pizza365.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import com.devcamp.pizza365.entity.Customer;
import com.devcamp.pizza365.entity.Order;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
    @Query(value = "SELECT COUNT(id) AS customer_num, country FROM `customers` WHERE country like :country", nativeQuery = true)
	long countCustomersByCountry(@Param("country") String country);
}
